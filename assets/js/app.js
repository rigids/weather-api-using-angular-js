/*
	App to display weather either by GeoLocation OR by zipcode using openweathermap.org API
	If no user input (either accept or deny from user) within 5second is not recieved 
	This app will prompt for Zipcode 
*/

var app = angular.module('weatherApp', []);
var latLng,lat,geoOptions = {
        enableHighAccuracy: false,
        timeout: 5000, // Wait 5 seconds
        maximumAge: 300000 //  Valid for 5 minutes
    };
 app.controller('weatherCtrl', ['$scope', 'weatherService', function($scope, weatherService) {
		$scope.formshow=false;
		$scope.resultshow=false;
		
		// Function to handle when there is no geolocation available
		$scope.userLocationNotFound = function (error) {
            switch (error.code) {
                case error.PERMISSION_DENIED:
				case 7878:
					// 7878 is a special case no user response for geolocation show input box for zipcode
                     $scope.formshow=true;
					 $scope.findWeather = function(zip) {
						 $scope.place = '';
						 fetchWeather(zip,1);
						 $scope.resultshow=true;
					 };
					 $scope.$apply();
                    break;
                case error.POSITION_UNAVAILABLE:
                    $scope.error = "Location information is unavailable."
                    break;
                case error.TIMEOUT:
                    $scope.error = "The request to get user location timed out."
                    break;
                case error.UNKNOWN_ERROR:
                    $scope.error = "An unknown error occurred."
                    break;
				
            }
            $scope.$apply();
        }
		
		// function to get gelocation lat and long
		$scope.showPosition = function (position) {
            lat = position.coords.latitude;
            $scope.lng = position.coords.longitude;
			latLng = {
				userlat: lat, // user location lat 
				userlng: $scope.lng  // user location lng
			};
            $scope.accuracy = position.coords.accuracy;
			 //fetchWeather('160047');
			 if(lat){
				$scope.resultshow=true;
				fetchWeather(latLng,0); // call the function if latLng is available
				$scope.$apply();
			 }
        }
        $scope.getLocation = function () {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition($scope.showPosition, $scope.userLocationNotFound,geoOptions);
            }
            else {
                alert( "Geolocation is not supported by this browser.");
            }
        }
 
      // function to call http request 
     function fetchWeather(zip,param) {
         weatherService.getWeather(zip,param).then(function(data) { 
             $scope.place = data;
             $scope.temp = data.list[0].temp;
			 $scope.tempimagefilepath = 'http://openweathermap.org/img/w/'+data.list[0].weather[0].icon+'.png';
         });
     }
	 
	 $scope.getLocation(); // get geolocation
	 /*
	 As clicking Cross on top right is treated as Not now option add a hack to 
	 still get the rest of app working
	 */
	 setTimeout(function () {
    if(!lat){
			alert("Please provide your Zipcode to get Weather information");
			$scope.userLocationNotFound({'code':7878});
		}else{
			console.log("Location was set");
		}
	}, geoOptions.timeout ); // Wait extra second
	 

 }]);
  
 app.factory('weatherService', ['$http', '$q', function($http, $q) {
	 function getWeather(searchFor,searchBy) {
		 var deferred = $q.defer();
		 if(searchBy==0){
			// serach by Latitude and longitude
			 url='http://api.openweathermap.org/data/2.5/forecast/daily?lat='+latLng.userlat+'&lon='+latLng.userlng+'&mode=json&units=metric&APPID=638213cb2e8e3010a6c672a1f3ecda9d';;
		 }
		 else{
			 // search by zip code 
			 url='http://api.openweathermap.org/data/2.5/forecast/daily?zip='+searchFor+'&mode=json&units=metric&APPID=638213cb2e8e3010a6c672a1f3ecda9d';
		 }
			 $http.get(url)		 
			 .success(function(data) {
				 deferred.resolve(data);
			 })
			 .error(function(err) {
				 alert('An Error Occured');
				 deferred.reject(err);
			 });
			 return deferred.promise;
		 }
		 return {
			 getWeather: getWeather
		 };
	 }]);